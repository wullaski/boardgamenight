boardgamenight
==============

A night for boardgames.

https://wullaski.gitlab.io/boardgamenight

What it is:
- Displays a countdown for the night in question
- Lists available boardgames
- Lists players and what game they want to play

What it isn't:
- Bug free
